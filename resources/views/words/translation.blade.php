@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col">
            <h2 class="text-center">{{$word->title}}</h2><br>
            <h3>EN: {{$word->translate('en')->title ?? ''}}</h3>
            <h3>FR: {{$word->translate('fr')->title ?? ''}}</h3>
            <h3>KG: {{$word->translate('kg')->title ?? ''}}</h3>
            <h3>KZ: {{$word->translate('kz')->title ?? ''}}</h3>
            <br>
            @can('update', $word)
            <a href="{{route('words.show', compact('word'))}}">
               <b>@lang('messages.add')</b>
            </a>
            @endcan
        </div>
    </div>
@endsection
