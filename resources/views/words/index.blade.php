@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">№</th>
                    <th scope="col">@lang('messages.words')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($words as $word)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>
                        <a href="{{route('words.translation', ['word'=>$word])}}">
                            {{$word->title}}
                        </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @can('create', \App\Models\Word::class)
        <div class="col">
            <form action="{{route('words.store')}}" method="post">
                @csrf
                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>

                @error('title')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror <br>

                <button class="btn btn-sm btn-outline-success">@lang('messages.added')</button>
            </form>
        </div>
        @endcan
    </div>
@endsection
