@extends('layouts.app')

@section('content')

    <form action="{{route('words.translate', compact('word'))}}" method="post">
        @csrf
        <div class="form-group row">

            <div class="col-md-8">
                <label for="title">EN</label>
                <input class="form-control @error('title') is-invalid @enderror" type="text"
                       id="title" name="entitle"
                       value="{{$word->translate('en')->title ?? ''}}"/>
                @error('title')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
            </div>

            <div class="col-md-8">
                <label for="title">FR</label>
                <input class="form-control @error('title') is-invalid @enderror" type="text"
                       id="title" name="frtitle"
                       value="{{$word->translate('fr')->title ?? ''}}"/>
                @error('title')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
            </div>

            <div class="col-md-8">
                <label for="title">KG</label>
                <input class="form-control @error('title') is-invalid @enderror" type="text"
                       id="title" name="kgtitle"
                       value="{{$word->translate('kg')->title ?? ''}}"/>
                @error('title')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
            </div>

            <div class="col-md-8">
                <label for="title">KZ</label>
                <input class="form-control @error('title') is-invalid @enderror" type="text"
                       id="title" name="kztitle"
                       value="{{$word->translate('kz')->title ?? ''}}"/>
                @error('title')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
            </div>
        </div>
        <button class="btn btn-sm btn-outline-success">@lang('messages.added')</button>
    </form>


@endsection
