<?php

use App\Http\Controllers\LanguageSwitcherController;
use App\Http\Controllers\WordController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('language')->group(function (){
    Route::get('/', [WordController::class, 'index'])->name('home');
    Route::resource('words', WordController::class);
    Auth::routes();
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);
    Route::post('translate/{word}', [WordController::class, 'translate'])->name('words.translate');
    Route::get('translation/{word}', [WordController::class, 'translation'])->name('words.translation');
});

Route::get('language/{locale}', [LanguageSwitcherController::class, 'switcher'])
    ->name('language.switcher')->where('locale', 'en|ru');
