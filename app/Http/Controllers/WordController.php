<?php

namespace App\Http\Controllers;

use App\Http\Requests\WordRequest;
use App\Models\Word;
use App\Models\WordTranslation;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WordController extends Controller
{
    /**
     * @return Factory|View|Application
     */
    public function index()
    {
        $words = WordTranslation::all()->where('locale', 'ru');
        return view('words.index', compact('words'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param WordRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(WordRequest $request): RedirectResponse
    {
        $this->authorize('create', Word::class);
        $data = ['user_id' => Auth::id(),
            'ru' => ['title' => $request->input('title')],
        ];

        $translate = Word::create($data);
        $translate->save();
        return redirect()->route('words.index');
    }


    /**
     * @param Word $word
     * @return Application|Factory|View
     */
    public function show(Word $word)
    {
        return view('words.show', compact('word'));
    }


    /**
     * @param Request $request
     * @param Word $word
     */
    public function translate(Request $request, Word $word)
    {
        $this->authorize('update', $word);
        $data = ['user_id' => Auth::id(),
            'en' => ['title' => $request->input('entitle')],
            'fr' => ['title' => $request->input('frtitle')],
            'kg' => ['title' => $request->input('kgtitle')],
            'kz' => ['title' => $request->input('kztitle')],
        ];

        $word->update($data);
        return redirect()->route('words.index');
    }

    /**
     * @return Application|Factory|View
     */
    public function translation(Word $word)
    {
        return view('words.translation', compact('word'));
    }

}
