<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class WordFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
                'title' => $this->faker->unique()->randomElement(['играть', 'бегать', 'читать', 'молоко', 'красивый', 'плохой'])
        ];
    }
}
